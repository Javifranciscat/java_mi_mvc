package mvc.controllers;

import mvc.views.MainView;
import mvc.views.PersonaView;
import mvc.views.HomeView;
import mvc.models.PersonaModel;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Esta clase controla las acciones que realiza la ventana principal.
 * 
 * @author luisherrera
 */
public class MainViewController extends Controller{
    private MainView vista = new MainView();

    public MainViewController(MainView vista){
        this.vista = vista;
    }

    /**
     * inicia los parámetros del JFrame.
     */
    public void iniciaVistaPrincipal(){
        vista.setTitle("MVC APP");
        vista.setLocationRelativeTo(null);
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setVisible(true);

    }
    
    /**
     * Asigna los listeners a los componentes de la vista (Menu).
     */
    public void asignaListeners(){
        vista.jmMantPers.addActionListener(this);
        vista.jmHome.addMouseListener(this);

    }
    
    /**
     * Segundo método ejecutado luego del constructor. Esto arranca la
     * vista principal.
     */
    public void inicio(){
        iniciaVistaPrincipal();
        asignaListeners();
        cargaElHome();

    }
    
    public void cargaElPanelPersonas(){
        PersonaView personaView = new PersonaView();
        
        // se inicia el controlador de personas.
        PersonaController personaController = new PersonaController(
                personaView, 
                new PersonaModel()
        );
        personaController.iniciar();
        renovarPanel(personaView);
        
    }
    
    public void cargaElHome(){
        renovarPanel(new HomeView());
    }
    
    /**
     * Toma un panel y lo carga en el panel principal.
     * @param panel 
     */
    private void renovarPanel(JPanel panel){
        vista.jpPadre.removeAll();
        vista.jpPadre.add(panel);
        vista.revalidate();
        vista.repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // menú de mant personas
        if(e.getSource() == vista.jmMantPers) cargaElPanelPersonas();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.jmHome) cargaElHome();
    }
    
}
